# CsvTimestampParser

A simple Java command line application that can transform dates in CSV file into timestamp. User define
date format as a method input, so it can transform all formats of date.
An output file will have the same content as the original one except date transformed to timestamp.

## Usage

Command line method contains a following arguments: **'input' 'output' 'delimiter' startIndex dateIndex 'datePattern'**

**'input'** - path to the file that user wants to transform.

**'output'** - path to the file that application will write transformed CSV data. If the file doesn't exists, it will be
created. If file already exists, new file would have name with suffix in format ...[x], where x is first available
number of non existing file.

**'delimiter'** - a separator used to separate CSV data in input file.

**startIndex** - index of the first row that contains data with timestamp. All rows before startIndex will we copied to
the output file in original form.

**dateIndex** - index of column containing date that user wants to transform to timestamp.

**'datePattern'** - String representation of the input date format i.ex. "YYYY-MM-dd HH:mm:ss.SSS".

## License

```
Copyright 2018-present Dawid Tokarzewski

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```