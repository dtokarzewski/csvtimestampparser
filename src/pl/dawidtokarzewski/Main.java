package pl.dawidtokarzewski;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

public class Main {

    public static void main(String[] args) {

        BufferedReader reader;
        BufferedWriter writer;
        String delimiter;
        int startIndex;
        int dateIndex;
        DateFormat dateFormat;

        if (args.length != 6) {
            System.out.println("Invalid arguments! Expected format:\njava -jar MotionLogcatFormatter.jar 'input' " +
                    "'output' 'delimiter' startIndex dateIndex 'datePattern'");
            return;
        }

        try {
            reader = new BufferedReader(new FileReader(args[0]));
        } catch (FileNotFoundException e) {
            System.out.println("Invalid input file path! Expected format:\njava -jar" +
                    " MotionLogcatFormatter.jar 'input' 'output' 'delimiter' startIndex dateIndex 'datePattern'");
            return;
        }

        try {
            writer = new BufferedWriter(new FileWriter(getNonExistingOutput(args[1])));
        } catch (IOException e) {
            System.out.println("Invalid output file path! Expected format:\njava -jar" +
                    " MotionLogcatFormatter.jar 'input' 'output' 'delimiter' startIndex dateIndex 'datePattern'");
            closeStreams(reader, null);
            return;
        }

        delimiter = args[2];

        try {
            startIndex = Integer.valueOf(args[3]);
        } catch (NumberFormatException e) {
            System.out.println("Invalid startIndex! Expected format:\njava -jar" +
                    " MotionLogcatFormatter.jar 'input' 'output' 'delimiter' startIndex dateIndex 'datePattern'");
            closeStreams(reader, writer);
            return;
        }

        try {
            dateIndex = Integer.valueOf(args[4]);
        } catch (NumberFormatException e) {
            System.out.println("Invalid dateIndex! Expected format:\njava -jar" +
                    " MotionLogcatFormatter.jar 'input' 'output' 'delimiter' startIndex dateIndex 'datePattern'");
            closeStreams(reader, writer);
            return;
        }

        try {
            dateFormat = new SimpleDateFormat(args[5], Locale.US);
        } catch (IllegalArgumentException e) {
            System.out.println("Invalid date pattern! Expected format:\njava -jar" +
                    " MotionLogcatFormatter.jar 'input' 'output' 'delimiter' startIndex dateIndex 'datePattern'");
            closeStreams(reader, writer);
            return;
        }

        int lineIndex = -1;
        String inLine;
        try {
            while((inLine = reader.readLine()) != null) {
                lineIndex++;
                if (lineIndex >= startIndex) {
                    String[] values = inLine.split(delimiter);
                    values[dateIndex] = String.valueOf(dateFormat.parse(values[dateIndex]).getTime());
                    writer.write(String.join(";", values));
                    writer.newLine();
                } else {
                    writer.write(inLine);
                    writer.newLine();
                }
                System.out.print(".");
            }

            System.out.println("\nSucceeded to parse " + (lineIndex + 1) + " lines.");
        } catch (IndexOutOfBoundsException e) {
            System.out.println("Failed trying parse data in line: " + lineIndex + ". Invalid date index.");
        }  catch (ParseException e) {
            System.out.println("Failed trying parse data in line: " + lineIndex + ". Invalid date format.");
        } catch (IOException e) {
            System.out.println("Failed trying parse data in line: " + lineIndex);
            e.printStackTrace();
        }

        closeStreams(reader, writer);
    }

    private static String getNonExistingOutput(String output) {
        int existingCount = 0;
        String outputUnique = output;
        while ((new File(outputUnique)).exists()) {
            existingCount++;
            int dotIndex = output.lastIndexOf('.');
            outputUnique = output.substring(0, dotIndex) + "[" + existingCount + "]" + output.substring(dotIndex, output.length());
        }
        return outputUnique;
    }

    private static void closeStreams(BufferedReader reader, BufferedWriter writer) {
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (reader != null) {
            try {
                reader.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
